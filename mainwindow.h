#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "newsourcedialogwindow.h"
#include "database.h"
#include <QMainWindow>
#include <QTextEdit>
#include <QSqlRelationalTableModel>
#include <QGroupBox>
#include <QComboBox>
#include <QPlainTextEdit>
#include <QVBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QHeaderView>
#include <QModelIndex>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    database db;
    QSqlRelationalTableModel *dataModel;
    QComboBox *nameView;
    QTableView *sourceListView;
    QPlainTextEdit *sourceTextView;
    QMenu *fileMenu;

    QGroupBox* createSourceListBox();
    QGroupBox* createSourceNameBox();
    QGroupBox* createSourceTextBox();


signals:
    void dataRowClicked();

private slots:

    void updateData();
    void showText(const QModelIndex &index);
    void on_newSource_clicked();
    void changePaper(int row);



private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
