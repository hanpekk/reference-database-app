#ifndef DATABASE_H
#define DATABASE_H
#include <QString>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QDebug>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#include <QDate>
#include <QTableView>
#include <QSqlTableModel>


class database : public QObject
{
    Q_OBJECT
public:
    database();


private:
    QSqlDatabase db;

};

#endif // DATABASE_H
