# Simple source reference tool

This tool is designed for reading and editing academic references and sources. The user adds the necessary data which the tool simply organizes to easily read format. The data is stored in a local SQL database.

At this point the tool is specifically designed for newspapers and their contents.

This program was made as practice in C++ and Qt.

## Author

Hannu-Pekka Kangas

##  License

[MIT](https://choosealicense.com/licenses/mit/)
