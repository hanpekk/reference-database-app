#include "newsourcedialogwindow.h"
#include "ui_newsourcedialogwindow.h"


newSourceDialogWindow::newSourceDialogWindow(QSqlRelationalTableModel *dataModel, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newSourceDialogWindow)
{
    model = dataModel;
    ui->setupUi(this);

}

newSourceDialogWindow::~newSourceDialogWindow()
{
    delete ui;
}

QString newSourceDialogWindow::returnName()
{
    return ui->sourceEdit->toPlainText();
}
QDate newSourceDialogWindow::returnDate(){
    return ui->dateEdit->date();
}
QString newSourceDialogWindow::returnAuthor(){
    return ui->authorEdit->toPlainText();
}
QString newSourceDialogWindow::returnTitle(){
    return ui->titleEdit->toPlainText();
}
QString newSourceDialogWindow::returnDesc(){
    return ui->descEdit->toPlainText();
}
QString newSourceDialogWindow::returnText(){
    return ui->textEdit->toPlainText();
}
bool newSourceDialogWindow::checkIfValidData()
{bool valid= false;
    if ((!(this->returnName()==""||this->returnName()==" "))
            && (!(this->returnAuthor()==""||this->returnAuthor()==" "))
            && (!(this->returnTitle()==""||this->returnTitle()==" "))
            && (!(this->returnDesc()==""||this->returnDesc()==" "))
            && (!(this->returnText()==""||this->returnText()==" ")))

        valid=true;

    return valid;
}

newspaper *newSourceDialogWindow::createNewspaper()
{
    newspaper *news=new newspaper(newSourceDialogWindow::returnName(),
                                  findNewspaperId(returnName()),newSourceDialogWindow::returnDate(),
                                  newSourceDialogWindow::returnTitle(), newSourceDialogWindow::returnAuthor(),
                                  newSourceDialogWindow::returnDesc(), newSourceDialogWindow::returnText());

    return news;
}

int newSourceDialogWindow::findNewspaperId(const QString &name)
{
    int id=0;

    QSqlQuery query;

    query.prepare("SELECT * FROM newspapers WHERE name = :search ");
    query.bindValue(":search",name);
    query.exec();

    if(query.next()){
        id=query.value(0).toInt();
        qDebug()<<id;
    }

    if(id==0){
        query.prepare("INSERT INTO newspapers (name)" "VALUES (:name)");
        query.bindValue(":name",name);
        query.exec();

        query.prepare("SELECT * FROM newspapers WHERE name = :search ");
        query.bindValue(":search",name);
        query.exec();

        if(query.next()){
            id=query.value(0).toInt();
            qDebug()<<id;
        }

    }
    return id;

}


bool newSourceDialogWindow::addNewspaperToDatabase(newspaper &newspaper){
    bool success= false;
    QSqlQuery query;


    query.prepare("INSERT INTO articles (newspaperid,date, author, title, description, text)" "VALUES (:newspaperid,:date,:author,:title,:description, :text)");
    query.bindValue(":newspaperid",newspaper.newspaperid);
    query.bindValue(":date",newspaper.date);
    query.bindValue(":author",newspaper.author);
    query.bindValue(":title",newspaper.title);
    query.bindValue(":description",newspaper.description);
    query.bindValue(":text",newspaper.text);


    if(query.exec()){
        success=true;
    }else{
        qDebug()<<"addNewspaper error:"<<query.lastError();
    }
    emit dataChanged();

    return success;
}


//cancel button
void newSourceDialogWindow::on_buttonBox_rejected()
{
    close();
}


//ok button
void newSourceDialogWindow::on_buttonBox_accepted()
{
    if(this->checkIfValidData()){
        newSourceDialogWindow::addNewspaperToDatabase(*newSourceDialogWindow::createNewspaper());
        //  sends signal for func database::addNewspaper to trigger
        close();


    }else{
        QMessageBox msg;
        msg.setText("Cannot write into database. Make sure ALL fields contain data.");
        msg.setWindowTitle("ERROR");
        msg.exec();

    }}




