#ifndef NEWSOURCEDIALOGWINDOW_H
#define NEWSOURCEDIALOGWINDOW_H
#include <QDialog>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRelationalTableModel>
#include <QSqlRecord>
#include "newspaper.h"



namespace Ui {
class newSourceDialogWindow;
}

class newSourceDialogWindow : public QDialog
{
    Q_OBJECT

public:
    explicit newSourceDialogWindow(QSqlRelationalTableModel *dataModel, QWidget *parent = nullptr);
    ~newSourceDialogWindow();
private:

    QString returnName();
    QDate returnDate();
    QString returnAuthor();
    QString returnTitle();
    QString returnDesc();
    QString returnText();
    bool checkIfValidData();
    newspaper* createNewspaper();
    bool addNewspaperToDatabase(newspaper &newspaper);

    QSqlRelationalTableModel *model;

signals:
    void dataChanged();

public slots:
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
    int findNewspaperId(const QString &name);


private:
    Ui::newSourceDialogWindow *ui;
};

#endif // NEWSOURCEDIALOGWINDOW_H
