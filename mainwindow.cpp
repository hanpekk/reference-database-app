#include "mainwindow.h"
#include "./ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    //this takes data from the SQL database
    dataModel=new QSqlRelationalTableModel(this);
    dataModel->setTable("articles");
    dataModel->setRelation(1,QSqlRelation("newspapers", "id","name"));
    dataModel->setEditStrategy(QSqlRelationalTableModel::OnManualSubmit);
    dataModel->select();


    QGroupBox *nameBox = createSourceNameBox();
    QGroupBox *sourceBox = createSourceListBox();
    QGroupBox *textBox = createSourceTextBox();

    ui->gridLayout->addWidget(nameBox);
    ui->gridLayout->addWidget(sourceBox);
    ui->gridLayout->addWidget(textBox);



    resize(800,600);

}

MainWindow::~MainWindow()
{
    delete ui;
}


//creates and returns a box whih shows data from SQl database using the dataModel created in MainWindow constructor
QGroupBox* MainWindow::createSourceListBox()
{
    QGroupBox *box = new QGroupBox(tr("Source"));

    sourceListView = new QTableView;
    sourceListView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    sourceListView->setSortingEnabled(true);
    sourceListView->setSelectionBehavior(QAbstractItemView::SelectRows);
    sourceListView->setSelectionMode(QAbstractItemView::SingleSelection);
    sourceListView->setShowGrid(false);
    sourceListView->verticalHeader()->hide();
    sourceListView->setAlternatingRowColors(true);


    sourceListView->setModel(dataModel);

    sourceListView->hideColumn(0);

    connect(sourceListView, &QTableView::clicked, this, &MainWindow::showText);


    QGridLayout *layout = new QGridLayout;

    layout->addWidget(sourceListView, 0, { });
    box->setLayout(layout);

    return box;
}

QGroupBox *MainWindow::createSourceNameBox()
{
    nameView = new QComboBox;
    nameView->setPlaceholderText(QStringLiteral("--Select newspaper--"));
    nameView->setModel(dataModel->relationModel(1));
    nameView->setCurrentIndex(-1);
    //box uses column "name" (index 1)
    nameView->setModelColumn(1);


    connect(nameView, &QComboBox::currentIndexChanged, this, &MainWindow::changePaper);

    QGroupBox *box = new QGroupBox(tr("Newspaper"));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(nameView, 0, 0);
    box->setLayout(layout);

    return box;
}

QGroupBox *MainWindow::createSourceTextBox()
{
    QGroupBox *box = new QGroupBox(tr("Text"));
    sourceTextView=new QPlainTextEdit;
    sourceTextView->setReadOnly(true);

    QGridLayout *layout = new QGridLayout;
    box->setLayout(layout);
    layout->addWidget(sourceTextView);

    return box;
}


void MainWindow::updateData(){


    dataModel->setTable("articles");
    dataModel->setRelation(1,QSqlRelation("newspapers", "id","name"));
    dataModel->select();
    nameView->setModel(dataModel->relationModel(1));

}


void MainWindow::on_newSource_clicked()
{
    newSourceDialogWindow sourceDialog(MainWindow::dataModel,this);
    connect(&sourceDialog,&newSourceDialogWindow::dataChanged,this,&MainWindow::updateData);
    sourceDialog.exec();
}

void MainWindow::showText(const QModelIndex &index)
{
    if(index.isValid()){
        QString cellText = index.data().toString();

        MainWindow::sourceTextView->setPlainText(cellText);

    }

}

void MainWindow::changePaper(int row)
{
    sourceListView->hideColumn(1);
    if (row > -1) {
        QModelIndex index = dataModel->relationModel(1)->index(row, 1);
        dataModel->setFilter("name = '" + index.data().toString() + '\'') ;

    } else if (row == -1) {
        dataModel->setFilter(QString());

    } else {
        return;
    }

}



