#ifndef NEWSPAPER_H
#define NEWSPAPER_H
#include <QString>
#include <QDate>


class newspaper: public QObject
{
    Q_OBJECT
public:
    newspaper(QString name,int newspaperid,QDate date,QString author,QString title,QString description,QString text);

    int newspaperid;
    QString name;
    QDate date;
    QString author;
    QString title;
    QString description;
    QString text;
};

#endif // NEWSPAPER_H
